# Cookbook:: template
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'template::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      # SoloRunner is recommended because it is faster, but if you need a
      # feature that is not supported by SoloRunner then try ServerRunner.
      ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '22.04',
      ).converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'has correct default attributes' do
      expect(chef_run.node['template']).to eq('set me')
    end
  end
end
