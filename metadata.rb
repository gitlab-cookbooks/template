name             'template'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Cookbook template for GitLab cookbooks'
version          '0.0.2'
chef_version     '>= 12.1'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users'

supports 'ubuntu', '= 16.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
